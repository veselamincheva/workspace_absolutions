package com.absolutions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author Vesela Mincheva
 * @created 3 May 2017
 * @company ABSolutions
 */
@SpringBootApplication
public class JobtoonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobtoonsApplication.class, args);
	}
}
