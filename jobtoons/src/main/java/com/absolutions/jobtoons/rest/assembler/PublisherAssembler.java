/**
 * 
 */
package com.absolutions.jobtoons.rest.assembler;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Service;

import com.absolutions.jobtoons.model.Publisher;
import com.absolutions.jobtoons.rest.PublisherController;
import com.absolutions.jobtoons.rest.resource.CompanyResource;
import com.absolutions.jobtoons.rest.resource.PublisherResource;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@Service
public class PublisherAssembler
		extends EmbeddableResourceAssemblerSupport<Publisher, PublisherResource, PublisherController> {

	@Autowired
	private CompanyAssembler companyAssembler;

	@Autowired
	public PublisherAssembler(EntityLinks entityLinks, RelProvider relProvider) {
		super(entityLinks, relProvider, PublisherController.class, PublisherResource.class, Publisher.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public PublisherResource toResource(Publisher entity) {
		PublisherResource resource = createResourceWithId(entity.getId(), entity);
		BeanUtils.copyProperties(entity, resource);
		String companyRel = relProvider.getItemResourceRelFor(CompanyResource.class);
		if (entity.getCompany() != null) {
			resource.add(companyAssembler.linkToSingleResource(entity.getCompany()).withRel(companyRel));
		}
		return resource;
	}

	@Override
	public Link linkToSingleResource(Publisher entity) {
		return entityLinks.linkToSingleResource(PublisherResource.class, entity.getId());
	}

}
