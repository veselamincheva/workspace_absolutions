package com.absolutions.jobtoons.rest;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import com.absolutions.jobtoons.model.DomainObject;

public abstract class ResourceHandlingUtils {

	public static <T extends DomainObject> T entityOrNotFoundException(T entity) {
		if (entity == null) {
			throw new ResourceNotFoundException();
		}
		return entity;
	}
}
