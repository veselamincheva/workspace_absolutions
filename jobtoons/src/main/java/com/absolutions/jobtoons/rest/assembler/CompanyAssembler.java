/**
 * 
 */
package com.absolutions.jobtoons.rest.assembler;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import com.absolutions.jobtoons.model.Company;
import com.absolutions.jobtoons.rest.CompanyController;
import com.absolutions.jobtoons.rest.resource.CompanyResource;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@Service
public class CompanyAssembler extends EmbeddableResourceAssemblerSupport<Company, CompanyResource, CompanyController> {

	@Autowired
	private PublisherAssembler publisherAssembler;

	@Autowired
	public CompanyAssembler(EntityLinks entityLinks, RelProvider relProvider) {
		super(entityLinks, relProvider, CompanyController.class, CompanyResource.class, Company.class);
	}

	@Override
	public CompanyResource toResource(Company entity) {
		final CompanyResource resource = createResourceWithId(entity.getId(), entity);
		BeanUtils.copyProperties(entity, resource);
		// for (Publisher publisher : entity.getPublishers()) {

		// Link link = publisherAssembler.linkToSingleResource(publisher);
		// link.
		resource.add(ControllerLinkBuilder
				.linkTo(ControllerLinkBuilder.methodOn(CompanyController.class).loadPublishers(entity.getId()))
				.withRel("publishers"));

		// }

		return resource;
	}

	@Override
	public Link linkToSingleResource(Company entity) {
		return entityLinks.linkToSingleResource(CompanyResource.class, entity.getId());
	}

}
