/**
 * 
 */
package com.absolutions.jobtoons.rest;

import java.net.URI;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.UriToEntityConverter;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.absolutions.jobtoons.model.Company;
import com.absolutions.jobtoons.model.Publisher;
import com.absolutions.jobtoons.repository.PublisherRepository;
import com.absolutions.jobtoons.rest.assembler.CompanyAssembler;
import com.absolutions.jobtoons.rest.assembler.PublisherAssembler;
import com.absolutions.jobtoons.rest.resource.CompanyResource;
import com.absolutions.jobtoons.rest.resource.PublisherResource;

/**
 * @author Vesela Mincheva
 * @created 8 May 2017
 * @company ABSolutions
 */
@ExposesResourceFor(CompanyResource.class)
@RequestMapping("/companies")
@RestController
public class CompanyController extends AbstractCRUDController<Company, CompanyResource> {

	@Autowired
	private UriToEntityConverter uriToEntityConverter;

	@Autowired
	private PublisherRepository publisherRepository;

	@Autowired
	private PublisherAssembler publisherAssembler;

	@Autowired
	public CompanyController(CrudRepository<Company, String> repo, CompanyAssembler assembler) {
		super(repo, assembler);
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value = "/{id}/publishers", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Resources<PublisherResource>> loadPublishers(@PathVariable String id) {
		Company c = new Company();
		c.setId(id);

		List<Publisher> publishers = publisherRepository.findByCompany(c);

		final Resources<PublisherResource> wrapped = publisherAssembler.toEmbeddedList(publishers);
		return ResponseEntity.ok(wrapped);
	}

	@RequestMapping(value = "/{id}/publishers", method = RequestMethod.PUT, consumes = "text/uri-list")
	public @ResponseBody ResponseEntity<Resources<PublisherResource>> addPublisher(@PathVariable String id,
			@RequestBody String publisherURI) {
		Company c = ResourceHandlingUtils.entityOrNotFoundException(repo.findOne(id));

		URI uri = URI.create(publisherURI);
		TypeDescriptor td = TypeDescriptor.valueOf(Publisher.class);
		Publisher publisher = (Publisher) uriToEntityConverter.convert(uri, td, td);
		publisher.setCompany(c);
		c.getPublishers().add(publisher);
		repo.save(c);

		final Resources<PublisherResource> wrapped = publisherAssembler.toEmbeddedList(c.getPublishers());
		return ResponseEntity.ok(wrapped);
	}

	@RequestMapping(value = "/{id}/publishers", method = RequestMethod.DELETE, consumes = "text/uri-list")
	public @ResponseBody ResponseEntity<Resources<PublisherResource>> deletePublisher(@PathVariable String id,
			@RequestBody String publisherURI) {
		Company c = ResourceHandlingUtils.entityOrNotFoundException(repo.findOne(id));

		URI uri = URI.create(publisherURI);
		TypeDescriptor td = TypeDescriptor.valueOf(Publisher.class);
		Publisher publisher = (Publisher) uriToEntityConverter.convert(uri, td, td);

		for (Iterator<Publisher> iterator = c.getPublishers().iterator(); iterator.hasNext();) {
			Publisher p = iterator.next();
			if (p.getId().equals(publisher.getId())) {
				iterator.remove();
				publisherRepository.delete(p);
				break;
			}

		}

		final Resources<PublisherResource> wrapped = publisherAssembler.toEmbeddedList(c.getPublishers());
		return ResponseEntity.ok(wrapped);
	}
}
