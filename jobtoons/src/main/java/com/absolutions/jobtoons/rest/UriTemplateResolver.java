/**
 * 
 */
package com.absolutions.jobtoons.rest;

import java.util.Map;

import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.core.AnnotationMappingDiscoverer;
import org.springframework.hateoas.core.DummyInvocationUtils;
import org.springframework.hateoas.core.MappingDiscoverer;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Vesela Mincheva
 * @created 11 May 2017
 * @company ABSolutions
 */
public class UriTemplateResolver {

	private static final MappingDiscoverer DISCOVERER = new AnnotationMappingDiscoverer(RequestMapping.class);

	private final Object invocationValue;

	private UriTemplateResolver(Object invocationValue) {
		this.invocationValue = invocationValue;
	}

	public static interface ControllerClosure<T> {

		void accept(T controller);

	}

	public static <T> UriTemplateResolver template(Class<T> controller, ControllerClosure<T> consumer) {
		T invocationValue = DummyInvocationUtils.methodOn(controller);
		consumer.accept(invocationValue);

		return new UriTemplateResolver(invocationValue);
	}

	public <T> Map<String, String> resolveVariables(String uri) {
		DummyInvocationUtils.LastInvocationAware invocations = (DummyInvocationUtils.LastInvocationAware) invocationValue;
		DummyInvocationUtils.MethodInvocation invocation = invocations.getLastInvocation();

		String mapping = DISCOVERER.getMapping(invocation.getTargetType(), invocation.getMethod());
		UriTemplate template = new UriTemplate(mapping);

		return null;// template.match(uri);
	}
}
