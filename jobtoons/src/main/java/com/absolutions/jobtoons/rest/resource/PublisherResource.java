/**
 * 
 */
package com.absolutions.jobtoons.rest.resource;

import org.springframework.hateoas.core.Relation;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@Relation(value = "publisher", collectionRelation = "publishers")
public class PublisherResource extends PersonResource {

	// public PublisherResource(String firstName, String lastName, String email,
	// String password, Address address) {
	// super(firstName, lastName, email, password, address);
	// // TODO Auto-generated constructor stub
	// }

}
