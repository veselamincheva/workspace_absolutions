package com.absolutions.jobtoons.rest.assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.core.EmbeddedWrapper;
import org.springframework.hateoas.core.EmbeddedWrappers;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.absolutions.jobtoons.model.DomainObject;
import com.absolutions.jobtoons.rest.AbstractCRUDController;

public abstract class EmbeddableResourceAssemblerSupport<D extends DomainObject, R extends ResourceSupport, C extends AbstractCRUDController<D, R>>
		extends ResourceAssemblerSupport<D, R> {

	protected final RelProvider relProvider;

	protected final EntityLinks entityLinks;

	protected final Class<C> controllerClass;

	protected final Class<D> domainType;

	public EmbeddableResourceAssemblerSupport(final EntityLinks entityLinks, final RelProvider relProvider,
			Class<C> controllerClass, Class<R> resourceType, Class<D> domainType) {
		super(controllerClass, resourceType);
		this.entityLinks = entityLinks;
		this.relProvider = relProvider;
		this.controllerClass = controllerClass;
		this.domainType = domainType;
	}

	public List<EmbeddedWrapper> toEmbeddable(Iterable<D> entities) {
		final EmbeddedWrappers wrapper = new EmbeddedWrappers(true);
		final List<R> resources = toResources(entities);
		return resources.stream().map(a -> wrapper.wrap(a)).collect(Collectors.toList());
	}

	public EmbeddedWrapper toEmbeddable(D entity) {
		final EmbeddedWrappers wrapper = new EmbeddedWrappers(false);
		final R resource = toResource(entity);
		return wrapper.wrap(resource);
	}

	public Resources<R> toEmbeddedList(Iterable<D> entities) {
		final List<R> resources = toResources(entities);
		return new Resources<R>(resources, linkTo(controllerClass).withSelfRel());
	}

	public abstract Link linkToSingleResource(D entity);

	public D toDomainObject(R resource) {
		D domainObj = BeanUtils.instantiate(domainType);
		BeanUtils.copyProperties(resource, domainObj);
		return domainObj;
	}
}
