/**
 * 
 */
package com.absolutions.jobtoons.rest.resource;

import org.springframework.hateoas.core.Relation;

import com.absolutions.jobtoons.model.Address;
import com.absolutions.jobtoons.model.CareersPage;
import com.absolutions.jobtoons.model.ContactPerson;
import com.absolutions.jobtoons.model.EmployeesNumber;
import com.absolutions.jobtoons.model.Image;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@Relation(value = "company", collectionRelation = "companies")
public class CompanyResource extends ResourceWithEmbeddeds {

	private String name;

	private String industry;

	private EmployeesNumber emplNumber;

	private Address address;

	private ContactPerson contactPerson;

	private Image logo;

	private String color;

	private CareersPage careersPage;

	private String socialMediaLinks;

	// public CompanyResource(String name, String industry, EmployeesNumber
	// emplNumber, Address address,
	// ContactPerson contactPerson, Image logo, String color, CareersPage
	// careersPage, String socialMediaLinks) {
	// super();
	// this.name = name;
	// this.industry = industry;
	// this.emplNumber = emplNumber;
	// this.address = address;
	// this.contactPerson = contactPerson;
	// this.logo = logo;
	// this.color = color;
	// this.careersPage = careersPage;
	// this.socialMediaLinks = socialMediaLinks;
	// }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public EmployeesNumber getEmplNumber() {
		return emplNumber;
	}

	public void setEmplNumber(EmployeesNumber emplNumber) {
		this.emplNumber = emplNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Image getLogo() {
		return logo;
	}

	public void setLogo(Image logo) {
		this.logo = logo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public CareersPage getCareersPage() {
		return careersPage;
	}

	public void setCareersPage(CareersPage careersPage) {
		this.careersPage = careersPage;
	}

	public String getSocialMediaLinks() {
		return socialMediaLinks;
	}

	public void setSocialMediaLinks(String socialMediaLinks) {
		this.socialMediaLinks = socialMediaLinks;
	}

}
