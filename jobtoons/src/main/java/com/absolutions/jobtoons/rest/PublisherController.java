/**
 * 
 */
package com.absolutions.jobtoons.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.absolutions.jobtoons.model.Publisher;
import com.absolutions.jobtoons.rest.assembler.PublisherAssembler;
import com.absolutions.jobtoons.rest.resource.PublisherResource;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@ExposesResourceFor(PublisherResource.class)
@RequestMapping("/publishers")
@RestController
public class PublisherController extends AbstractCRUDController<Publisher, PublisherResource> {

	@Autowired
	public PublisherController(CrudRepository<Publisher, String> repo, PublisherAssembler assembler) {
		super(repo, assembler);
	}

}
