/**
 * 
 */
package com.absolutions.jobtoons.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.absolutions.jobtoons.model.DomainObject;
import com.absolutions.jobtoons.rest.assembler.EmbeddableResourceAssemblerSupport;

/**
 * @author Vesela Mincheva
 * @created 8 May 2017
 * @company ABSolutions
 */
public abstract class AbstractCRUDController<D extends DomainObject, R extends ResourceSupport> {

	private Logger logger = LoggerFactory.getLogger(AbstractCRUDController.class);

	protected CrudRepository<D, String> repo;

	protected EmbeddableResourceAssemblerSupport<D, R, ? extends AbstractCRUDController<D, R>> assembler;

	public AbstractCRUDController(CrudRepository<D, String> repo,
			EmbeddableResourceAssemblerSupport<D, R, ? extends AbstractCRUDController<D, R>> assembler) {
		this.repo = repo;
		this.assembler = assembler;
	}

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Resources<R>> listAll() {
		Iterable<D> all = this.repo.findAll();
		List<D> result = new ArrayList<>();
		all.forEach(result::add);
		final Resources<R> wrapped = assembler.toEmbeddedList(all);
		return ResponseEntity.ok(wrapped);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<R> create(@RequestBody R json) {
		logger.debug("create() with body {} of type {}", json, json.getClass());

		D domainObject = assembler.toDomainObject(json);

		domainObject = this.repo.save(domainObject);

		final R resource = assembler.toResource(domainObject);
		return ResponseEntity.ok(resource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<R> update(@PathVariable String id, @RequestBody R json) {
		logger.debug("create() with body {} of type {}", json, json.getClass());

		D domainObject = assembler.toDomainObject(json);
		domainObject.setId(id);

		domainObject = this.repo.save(domainObject);

		final R resource = assembler.toResource(domainObject);
		return ResponseEntity.ok(resource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<R> get(@PathVariable String id) {
		D entity = ResourceHandlingUtils.entityOrNotFoundException(repo.findOne(id));
		final R resource = assembler.toResource(entity);
		return ResponseEntity.ok(resource);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Void> delete(@PathVariable String id) {
		this.repo.delete(id);
		final HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.NO_CONTENT);// or ok
	}

}
