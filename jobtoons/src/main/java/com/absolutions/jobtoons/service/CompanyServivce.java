/**
 * 
 */
package com.absolutions.jobtoons.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.absolutions.jobtoons.model.Company;
import com.absolutions.jobtoons.repository.CompanyRepository;

/**
 * @author Vesela Mincheva
 * @created 4 May 2017
 * @company ABSolutions
 */

@Service
@Transactional
public class CompanyServivce {

	private final static Logger log = LoggerFactory.getLogger(CompanyServivce.class);

	@Autowired
	private CompanyRepository companyRepository;

	public Company create(Company company) {
		log.info("saving");
		// Company c = companyRepository.save(company);
		throw new RuntimeException("exception ");
	}

	public Company loadById(String id) {
		return null;// companyRepository.findOne(id);
	}

}
