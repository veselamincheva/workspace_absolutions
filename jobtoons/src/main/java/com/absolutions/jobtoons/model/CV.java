/**
 * 
 */
package com.absolutions.jobtoons.model;

import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class CV extends DomainObject {

	private static final long serialVersionUID = -613128396455763547L;

	@OneToOne
	private Person owner;

	private String headline;

	private String summary;

	private String highlights;// [a, b, c]

	private String story;

	@OneToMany
	@OrderBy("startDate DESC")
	private SortedSet<Experience> experiances = new TreeSet<>();

	private String languages; // [{name: English, level: ...},...]

	private String skills; // [a,b,c]

	@OneToMany
	@OrderBy("startDate DESC")
	private SortedSet<Experience> education = new TreeSet<>();

	private String courses; // [{name:.., description:...},...]

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getHighlights() {
		return highlights;
	}

	public void setHighlights(String highlights) {
		this.highlights = highlights;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public SortedSet<Experience> getExperiances() {
		return experiances;
	}

	public void setExperiances(SortedSet<Experience> experiances) {
		this.experiances = experiances;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public SortedSet<Experience> getEducation() {
		return education;
	}

	public void setEducation(SortedSet<Experience> education) {
		this.education = education;
	}

	public String getCourses() {
		return courses;
	}

	public void setCourses(String courses) {
		this.courses = courses;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}
}
