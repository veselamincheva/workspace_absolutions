/**
 * 
 */
package com.absolutions.jobtoons.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * @author Vesela Mincheva
 * @created 4 May 2017
 * @company ABSolutions
 */

@Entity
public class Company extends DomainObject {

	private static final long serialVersionUID = 8566276565700393056L;

	private String name;

	private String industry;

	private EmployeesNumber emplNumber;

	@Embedded
	private Address address;

	@Embedded
	private ContactPerson contactPerson;

	@OneToOne
	private Image logo;

	private String color;

	@OneToOne(fetch = FetchType.LAZY)
	private CareersPage careersPage;

	private String socialMediaLinks; // [{name: linkedin, url: ..}, ...]

	@OneToMany(mappedBy = "company", cascade = { CascadeType.REMOVE }, orphanRemoval = true)
	private Set<Publisher> publishers = new HashSet<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public EmployeesNumber getEmplNumber() {
		return emplNumber;
	}

	public void setEmplNumber(EmployeesNumber emplNumber) {
		this.emplNumber = emplNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Image getLogo() {
		return logo;
	}

	public void setLogo(Image logo) {
		this.logo = logo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public CareersPage getCareersPage() {
		return careersPage;
	}

	public void setCareersPage(CareersPage careersPage) {
		this.careersPage = careersPage;
	}

	public String getSocialMediaLinks() {
		return socialMediaLinks;
	}

	public void setSocialMediaLinks(String socialMediaLinks) {
		this.socialMediaLinks = socialMediaLinks;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}

}
