/**
 * 
 */
package com.absolutions.jobtoons.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class Ad extends DomainObject {

	private static final long serialVersionUID = -8913025385333904706L;

	private String name;

	private String description;

	private boolean status;

	private String location;

	private WorkingHours hours;

	private int salaryMin;

	private int salaryMax;

	private String industry;

	private String occupationalCategory;

	private String companyDescription;

	private String responsibilities;

	private String educationalRequarements;

	private String experianceRequarements;

	private String qualification;

	private String skills;

	private String benefits;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Company company;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public WorkingHours getHours() {
		return hours;
	}

	public void setHours(WorkingHours hours) {
		this.hours = hours;
	}

	public int getSalaryMin() {
		return salaryMin;
	}

	public void setSalaryMin(int salaryMin) {
		this.salaryMin = salaryMin;
	}

	public int getSalaryMax() {
		return salaryMax;
	}

	public void setSalaryMax(int salaryMax) {
		this.salaryMax = salaryMax;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getOccupationalCategory() {
		return occupationalCategory;
	}

	public void setOccupationalCategory(String occupationalCategory) {
		this.occupationalCategory = occupationalCategory;
	}

	public String getCompanyDescription() {
		return companyDescription;
	}

	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}

	public String getResponsibilities() {
		return responsibilities;
	}

	public void setResponsibilities(String responsibilities) {
		this.responsibilities = responsibilities;
	}

	public String getEducationalRequarements() {
		return educationalRequarements;
	}

	public void setEducationalRequarements(String educationalRequarements) {
		this.educationalRequarements = educationalRequarements;
	}

	public String getExperianceRequarements() {
		return experianceRequarements;
	}

	public void setExperianceRequarements(String experianceRequarements) {
		this.experianceRequarements = experianceRequarements;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
