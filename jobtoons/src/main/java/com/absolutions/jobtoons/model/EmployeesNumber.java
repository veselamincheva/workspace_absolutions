/**
 * 
 */
package com.absolutions.jobtoons.model;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
public enum EmployeesNumber {

	SMALL {

		@Override
		public String getValue() {
			return "1 - 50";
		}
	},
	MEDIUM {

		@Override
		public String getValue() {
			return "51 - 300";
		}
	},
	LARGE {

		@Override
		public String getValue() {
			return "301 - 1000";
		}
	};

	public abstract String getValue();
}
