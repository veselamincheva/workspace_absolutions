/**
 * 
 */
package com.absolutions.jobtoons.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.rest.core.annotation.RestResource;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@Entity
public class Publisher extends Person {

	private static final long serialVersionUID = 593575100231587025L;

	@ManyToOne
	@JoinColumn(name = "company_id")
	@RestResource(path = "comanies", rel = "companies")
	private Company company;

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
