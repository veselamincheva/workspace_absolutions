/**
 * 
 */
package com.absolutions.jobtoons.model;

import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class Application extends DomainObject {

	private static final long serialVersionUID = -5307686573729345849L;

	@ManyToOne(cascade = CascadeType.REFRESH)
	private Person applicant;

	@ManyToOne(cascade = CascadeType.REFRESH)
	private Ad ad;

	// TODO: could be the first message
	private String coverLetter;

	@OneToMany
	@OrderBy("createdDate DESC")
	private SortedSet<Message> messages = new TreeSet<>();

	public Person getApplicant() {
		return applicant;
	}

	public void setApplicant(Person applicant) {
		this.applicant = applicant;
	}

	public Ad getAd() {
		return ad;
	}

	public void setAd(Ad ad) {
		this.ad = ad;
	}

	public String getCoverLetter() {
		return coverLetter;
	}

	public void setCoverLetter(String coverLetter) {
		this.coverLetter = coverLetter;
	}

	public SortedSet<Message> getMessages() {
		return messages;
	}

	public void setMessages(SortedSet<Message> messages) {
		this.messages = messages;
	}

}
