/**
 * 
 */
package com.absolutions.jobtoons.model;

import javax.persistence.Embeddable;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Embeddable
public class ContactPerson {

	private String firstName;

	private String lastName;

	private String email;

	private String phone;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
