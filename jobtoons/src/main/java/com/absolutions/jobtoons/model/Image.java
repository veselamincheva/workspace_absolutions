/**
 * 
 */
package com.absolutions.jobtoons.model;

import javax.persistence.Entity;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class Image extends DomainObject {

	// id - name of the file on server file system

	private static final long serialVersionUID = 6472623335766634634L;

	private String originName;

	private String ownerType;

	private String ownerId;

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
}
