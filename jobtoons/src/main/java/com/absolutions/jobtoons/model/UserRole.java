/**
 * 
 */
package com.absolutions.jobtoons.model;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
public enum UserRole {

	EMPLOYEE, PUBLISHER;
}
