/**
 * 
 */
package com.absolutions.jobtoons.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class CareersPage extends DomainObject {

	private static final long serialVersionUID = -1019534863253596744L;

	private String why;

	private String highlights; // [{title: ..., description: ...}, ...]

	private String numbers;

	private String benefits;

	@OneToMany
	private Set<Quote> quotes = new HashSet<>();

	@OneToMany
	private Set<Image> gallery = new HashSet<>();

	public String getWhy() {
		return why;
	}

	public void setWhy(String why) {
		this.why = why;
	}

	public String getHighlights() {
		return highlights;
	}

	public void setHighlights(String highlights) {
		this.highlights = highlights;
	}

	public String getNumbers() {
		return numbers;
	}

	public void setNumbers(String numbers) {
		this.numbers = numbers;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public Set<Quote> getQuotes() {
		return quotes;
	}

	public void setQuotes(Set<Quote> quotes) {
		this.quotes = quotes;
	}

	public Set<Image> getGallery() {
		return gallery;
	}

	public void setGallery(Set<Image> gallery) {
		this.gallery = gallery;
	}

}
