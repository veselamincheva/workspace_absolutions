/**
 * 
 */
package com.absolutions.jobtoons.model;

import java.util.Date;

import javax.persistence.Entity;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class Experience extends DomainObject {

	private static final long serialVersionUID = -5992530868402051621L;

	private String companyName;

	private String position;

	private Date startDate;

	private Date endDate;

	private String location;

	private String description;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
