/**
 * 
 */
package com.absolutions.jobtoons.model;

import javax.persistence.Entity;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Entity
public class Message extends DomainObject {

	private static final long serialVersionUID = 9049503664127107405L;

	private String txt;

	public String getTxt() {
		return txt;
	}

	public void setTxt(String txt) {
		this.txt = txt;
	}
}
