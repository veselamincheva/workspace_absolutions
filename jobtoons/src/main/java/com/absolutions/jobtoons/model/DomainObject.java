/**
 * 
 */
package com.absolutions.jobtoons.model;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@MappedSuperclass
public abstract class DomainObject implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@JsonProperty(value = "id")
	private String id;

	@JsonIgnore
	@Transient
	private String currentUserId = "test";

	@JsonIgnore
	@Column(updatable = false)
	private String createdUserId;

	@JsonIgnore
	private String changedUserId;

	@JsonIgnore
	@Column(updatable = false)
	private ZonedDateTime createdDate = ZonedDateTime.now(ZoneId.of("UTC"));

	@JsonIgnore
	@Column(updatable = false)
	private ZonedDateTime changedDate = ZonedDateTime.now(ZoneId.of("UTC"));

	@PrePersist
	@PreUpdate
	protected void init() {
		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
		if (createdDate == null) {
			createdDate = now;
		}
		changedDate = now;
		if (createdUserId == null) {
			createdUserId = currentUserId;
		}
		changedUserId = currentUserId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DomainObject)) {
			return false;
		}
		DomainObject other = (DomainObject) obj;
		return getId().equals(other.getId());
	}

	@Override
	public int hashCode() {
		if (!StringUtils.hasText(getId())) {
			return super.hashCode();
		}
		return getId().hashCode();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Id: ").append(getId());
		return sb.toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getChangedUserId() {
		return changedUserId;
	}

	public void setChangedUserId(String changedUserId) {
		this.changedUserId = changedUserId;
	}

	public ZonedDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(ZonedDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public ZonedDateTime getChangedDate() {
		return changedDate;
	}

	public void setChangedDate(ZonedDateTime changedDate) {
		this.changedDate = changedDate;
	}

}
