/**
 * 
 */
package com.absolutions.jobtoons.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.absolutions.jobtoons.model.Company;
import com.absolutions.jobtoons.model.Publisher;

/**
 * @author Vesela Mincheva
 * @created 9 May 2017
 * @company ABSolutions
 */
@RestResource(exported = false)
public interface PublisherRepository extends JpaRepository<Publisher, String> {

	public List<Publisher> findByCompany(Company company);
}
