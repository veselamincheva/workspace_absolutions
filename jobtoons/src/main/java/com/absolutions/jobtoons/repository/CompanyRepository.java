/**
 * 
 */
package com.absolutions.jobtoons.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.absolutions.jobtoons.model.Company;

/**
 * @author Vesela Mincheva
 * @created 4 May 2017
 * @company ABSolutions
 */
@RestResource(exported = false)
public interface CompanyRepository extends JpaRepository<Company, String> {

}
