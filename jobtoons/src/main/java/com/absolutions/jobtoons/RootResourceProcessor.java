/**
 * 
 */
package com.absolutions.jobtoons;

import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import com.absolutions.jobtoons.rest.CompanyController;
import com.absolutions.jobtoons.rest.PublisherController;

/**
 * @author Vesela Mincheva
 * @created 7 May 2017
 * @company ABSolutions
 */
@Component
public class RootResourceProcessor implements ResourceProcessor<RepositoryLinksResource> {

	@Override
	public RepositoryLinksResource process(RepositoryLinksResource resource) {
		// resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(HomeController.class).getProducers())
		// .withRel("experience"));

		resource.add(ControllerLinkBuilder.linkTo(CompanyController.class).withRel("companies"));

		resource.add(ControllerLinkBuilder.linkTo(PublisherController.class).withRel("publishers"));
		return resource;
	}
}
