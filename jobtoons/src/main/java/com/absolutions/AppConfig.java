/**
 * 
 */
package com.absolutions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.repository.support.RepositoryInvokerFactory;
import org.springframework.data.rest.core.UriToEntityConverter;

/**
 * @author Vesela Mincheva
 * @created 8 May 2017
 * @company ABSolutions
 */
@Configuration
// @EnableHypermediaSupport(type = HypermediaType.HAL)
// @Import(RepositoryRestMvcConfiguration.class)
public class AppConfig {

	@Autowired
	private PersistentEntities entities;

	@Autowired
	private RepositoryInvokerFactory invokerFactory;

	@Autowired
	private Repositories repositories;

	@Bean
	public UriToEntityConverter convertor() {
		return new UriToEntityConverter(entities, invokerFactory, repositories);
	}

}
